![42 logo](https://i.imgur.com/lzsRhcd.gif)

# 42 Workspace 🗄

  List of all my projects at 42 
  (Click links to see the right branch)
  

## [1 - libft](https://github.com/flupine/42_workspace/tree/libft)
   
   Personal C library

## [2 - Get Next Line](https://github.com/flupine/42_workspace/tree/get_next_line)
   
   This project consist of writing a function get_next_line which get the next line in a file.

## [3 - Fillit](https://github.com/flupine/42_workspace/tree/fillit)
   
   Tetris like resolver.
